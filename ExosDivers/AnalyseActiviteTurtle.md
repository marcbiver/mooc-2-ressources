
# Analyse de l'activité [Turtle](https://gitlab.com/mooc-nsi-snt/mooc-2-_-pratique/-/raw/master/2_Mise-en-Pratique-Professionnelle/2.1_Penser-Concevoir-Elaborer/2.1.2_Exemples/activites_turtle.pdf?inline=false) récupérée sur le web  

## Objectifs
- Première introduction à Python
- Inititiation à la syntaxe du module Turtle

## Pré-requis à cette activité
- Compréhension de ce qu'est un langage de programmation
- Capacité à coder en langage procédural

## Durée de l'activité
Au jugé je dirais de l'ordre de 2 heures - mais dépendant évidemment du niveau des élèves  

## Exercices cibles
????

## Description du déroulement de l'activité
- Présentation du paragraphe introductif à l'oral
- Réalisation "à quatre mains" de l'exercice 1 puis 2
- Réalisation libre de l'exercice 3
- Devinette sur le 4
- etc.

## Anticipation des difficultés des élèves
- Ceux qui n'ont aucune expérience de code
- Ceux qui galèrent en géométrie
- Ceux qui n'osent pas / ont peur de faire une connerie.

## Gestion de l'hétérogénéïté
Elle est "built in" du fait des exos de différents niveaux bien étalonnés => chacun peut avancer à son rythme.