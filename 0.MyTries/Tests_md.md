#### Eléments de syntaxe de base  
Pour aller sur [google](https://www.google.com)  
*Juste une étoile* ça fait donc de l'italique?  
Pour voir une image on la __pointe__ avec son chemin relatif: ![blabliblou](pix/rosace.png)  
#### Formules mathématiques  
On peut aussi mettre des formules mathématiques complexes:  
$$\frac{\pi}{4} = \lim_{n\rightarrow\infty} \frac{\varphi(n)}{n^2}$$  
...ou plus simples:
$\forall n\in\mathbb{N}, n\geq 0$  
Voilà.
#### Essais juste pour passer à la ligne
Avec deux espaces et return:  
Juste avec return:
Avec deux return sans espaces

Et voilà.
#### Citations & tableaux (pourquoi pas?)
> Oh la belle citation !  
Et sa suite...

Et maintenant les tableaux:

| Langage | Inventeur  | Année |
| :------ | :-------:  | ----: |
| Python  | Guido van Rossum | 1991  |
| Java  | James Gosling  | 1995  |
| aligné à gauche | centré | aligné à droite |
#### Du code à présent
```python
    def foo(n):
        return n**2
````

Donc là on est sortis du code.
````
Là on redémarre un bloc sans spécifier de langage.
````

Bulletted lists
- Ca c'en est un
- Celui-là il vient après return sans espaces  
- là avec espaces
-- sans espaces deux tirets  
-- deux expaces deux tirets

Et on l'a terminé
